package test01;

import javax.swing.JOptionPane;

public class ControleFuncionario {

	
	public static void main (String[] args) {
		Funcionario funcionario;
		funcionario = new Funcionario();
		funcionario.setMatricula(JOptionPane.showInputDialog("Digite o matricula"));
		funcionario.setNome(JOptionPane.showInputDialog("Digite o nome"));
		funcionario.setCargo(JOptionPane.showInputDialog("cargo"));
		funcionario.setSalario(Double.parseDouble(JOptionPane.showInputDialog("Digite o salario")));
		
		
		JOptionPane.showMessageDialog(null, " Matricula: " + funcionario.getMatricula());
		JOptionPane.showMessageDialog(null, " Nome: " + funcionario.getNome());
		JOptionPane.showMessageDialog(null, " Cargo: " + funcionario.getCargo());
		JOptionPane.showMessageDialog(null, " Salario: " + funcionario.getSalario());
		
		funcionario.reajustarSalario();
		
		JOptionPane.showMessageDialog(null, " Salario reajustado: " + funcionario.getSalario());
		
	}
}

