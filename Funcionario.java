package test01;


public class Funcionario {

	
	  private String matricula;
	
	  private String nome;
	
	  private String cargo;
	
      private double salario;
	
	
	  public Funcionario() {
		
	
	}


	  public String getMatricula() {
	 	return matricula;
	}


	  public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	  public String getNome() {
		return nome;
	}


	  public void setNome(String nome) {
		this.nome = nome;
	}


	  public String getCargo() {
		return cargo;
	}


	  public void setCargo(String cargo) {
		this.cargo = cargo;
	}


	  public double getSalario() {
		return salario;
	}


	  public void setSalario(double salario) {
		this.salario = salario;
		
	}
      
	
	  public void reajustarSalario() {
		if (this.cargo.equalsIgnoreCase("Gerente ")) {
			this.salario = this.salario * 1.10;  // aumenta em 10%
		}else if (this.cargo.equalsIgnoreCase("vendedor")) {
			this.salario = this.salario * 1.05;  // aumenta em 5%
		} else {
			this.salario = this.salario * 1.01; // aumenta em 1%
			
			
		}
					
	}
		
	}
	
	
	
   
   


